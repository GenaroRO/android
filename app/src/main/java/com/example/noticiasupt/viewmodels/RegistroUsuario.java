package com.example.noticiasupt.viewmodels;

public class RegistroUsuario {
    public String estado;
    public String correo;
    public String password;
    public String detalle;

    public String getEstado() {
        return estado;
    }

    public String getCorreo() {
        return correo;
    }

    public String getPassword() {
        return password;
    }

    public String getDetalle() {
        return detalle;
    }
}
