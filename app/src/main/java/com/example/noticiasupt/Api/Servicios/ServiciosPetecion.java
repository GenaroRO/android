package com.example.noticiasupt.Api.Servicios;


import com.example.noticiasupt.viewmodels.RegistroUsuario;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ServiciosPetecion {
    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<RegistroUsuario> registrarUsuario(@Field("username") String correo, @Field("password") String contrasenia);
}
